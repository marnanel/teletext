import argparse
import teletext.unrender

def main():
    parser = argparse.ArgumentParser(
        description='convert images to teletext symbols')
    parser.add_argument(
            'image',
            type=str,
            help='image containing the pixels')
    parser.add_argument(
            '--braille', '-b',
            action='append_const', dest='form', const='braille',
            help='show output using braille symbols')
    parser.add_argument(
            '--zvbi', '-z',
            action='append_const', dest='form', const='zvbi',
            help='show output using ZVBI private-use symbols')
    parser.add_argument(
            '--teletext', '-t',
            action='append_const', dest='form', const='teletext',
            help=(
                'show output using teletext symbols. '
                'Bear in mind that this may involve printing U+007f, '
                'which most terminals interpret as a delete.'
                ))
    parser.add_argument(
            '--teletext-printable', '-T',
            action='append_const', dest='form', const='teletext-printable',
            help=(
                'show output using teletext symbols, but print '
                'backslashes in place of U+007f. This makes it more easily '
                'editable on Unicode systems. (default)'
                ))
    parser.add_argument(
            '--wide', '-w',
            action='store_true',
            help='override check for excessively wide images')
    args = parser.parse_args()

    if not args.form:
        args.form = ['teletext-printable']

    image=teletext.unrender.TeletextImage(args.image)

    if image.size[0]>100 and not args.wide:
        print(f'The image {args.image} is {image.size[0]}px wide, '
                'which seems excessive.')
        print('If you really wanted to use it, pass --wide to override '
                'this check.')
        return

    for form in args.form:
        print(image.as_text(form))

if __name__=='__main__':
    main()
