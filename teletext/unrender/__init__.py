from PIL import Image

class TeletextImage:

    FORM = {
            'braille': [0x01, 0x08, 0x02, 0x10, 0x04, 0x20],
            'zvbi': [0x01, 0x02, 0x04, 0x08, 0x10, 0x40],
            'teletext': [0x01, 0x02, 0x04, 0x08, 0x10, 0x40],
            'teletext-printable': [0x01, 0x02, 0x04, 0x08, 0x10, 0x40],
            }

    START = {
            'braille': 0x2800,
            'zvbi': 0xee00,
            'teletext': 0x20,
            'teletext-printable': 0x20,
            }

    def __init__(self, filename):
        self.image = Image.open(filename)
        self.size = self.image.size
        self.palette = self.image.getpalette()

    def get_pixel(self, x, y):
        try:
            colour = self.image.getpixel((x,y))
        except IndexError as e:
            return False

        if self.palette is not None:
            colour *= 3 # this API is ridiculous
            colour = (
                    self.palette[colour],
                    self.palette[colour+1],
                    self.palette[colour+2],
                    )

        if isinstance(colour, tuple):
            colour = sum(colour)/len(colour)

        return colour > 127

    def get_sixel(self, x, y):
        result = []
        x *= 2
        y *= 3
        for y2 in range(3):
            result.append(self.get_pixel(x, y+y2))
            result.append(self.get_pixel(x+1, y+y2))
        return result

    def get_sixel_symbol(self, x, y, form):
        sixel = self.get_sixel(x, y)

        result = self.START[form]
        for i, n in enumerate(sixel):
            if n:
                result += self.FORM[form][i]

        if form=='teletext-printable' and result==0x7f:
            return '\\'

        return chr(result)

    def as_text(self, form):

        if form not in self.FORM:
            raise ValueError(f'There is no form called {form}.')

        width, height = self.size

        width  = (width +1)//2
        height = (height+2)//3

        result = ''

        for y in range(height):
            line = ''
            for x in range(width):
                line += self.get_sixel_symbol(x, y, form)

            result += f'{line}\n'

        return result
