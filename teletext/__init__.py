__title__ = 'teletext'
__version__ = '0.0.1'
VERSION = __version__
__author__ = 'Marnanel Thurman'
__license__ = 'MIT'
__copyright__ = 'Copyright (c) 2022 Marnanel Thurman'

import sys
import types

__all__ = [
        'teletext.unrender',
        ]
