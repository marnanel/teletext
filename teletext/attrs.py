import logging

logger = logging.Logger("meefax.general")

class Attrs:

    BLACK = 0
    RED = 1
    GREEN = 2
    YELLOW = 3
    BLUE = 4
    MAGENTA = 5
    CYAN = 6
    WHITE = 7

    DOUBLE_TOP = 'd1'
    DOUBLE_BOTTOM = 'd2'
    FLASH = 'flash'
    SEP = 'sep'
    BOXED = 'boxed'
    GRAPHIC = 'graphic'
    CONCEAL = 'conceal'
    HOLD = 'hold'

    # In Unicode (and ASCII), codepoint 127 is a control character.
    # In videotex, # it isn't. So to avoid outputting a control character,
    # we substitute U+220E END OF PROOF, which looks similar.
    BLOCK_SYMBOL = 0x220e

    def __init__(self):
        self.has_flash = False
        self.has_boxes = False
        self.dh_this_line = False
        self.dh_class = None
        self.row = -1

        print("----")
        self.newline()

    def newline(self):
        self.fg = self.WHITE
        self.bg = self.BLACK

        print(self.dh_this_line)
        if self.dh_this_line:
            self.dh_class = self.DOUBLE_BOTTOM
        else:
            self.dh_class = self.DOUBLE_TOP
        print(self.dh_class)

        self.flags = set()

        self.column = 0
        self.row += 1
        self.dh_this_line = False

        logger.debug("%s: newline")

    @property
    def classes(self):
        result = {f'f{self.fg}', f'b{self.bg}'}
        result |= self.flags

        return ' '.join(sorted(result))

    def __repr__(self):
        result = f'[attrs;col={self.column};row={self.row};'
        result += ';'.join(sorted(self.classes))
        result += ']'
        return result

    def handle(self, c):

        self.column += 1
        
        if self.column>39:
            self.newline()

        c = self.normalise_char(c)

        classes_before = self.classes

        if c==0x00:
            pass # NUL
        elif c==0x01:
            self.fg = self.RED
        elif c==0x02:
            self.fg = self.GREEN
        elif c==0x03:
            self.fg = self.YELLOW
        elif c==0x04:
            self.fg = self.BLUE
        elif c==0x05:
            self.fg = self.MAGENTA
        elif c==0x06:
            self.fg = self.CYAN
        elif c==0x07:
            self.fg = self.WHITE

        elif c==0x08:
            self.flags.add(self.FLASH)
        elif c==0x09:
            self.flags.discard(self.FLASH)

        elif c==0x0a:
            self.flags.add(self.BOXED)
        elif c==0x0b:
            self.flags.discard(self.BOXED)

        elif c==0x0c:
            self.flags.discard(self.dh_class)
        elif c==0x0d:
            self.flags.add(self.dh_class)
            if self.dh_class==self.DOUBLE_TOP:
                self.dh_this_line = True

        elif c==0x0e:
            pass # SO
        elif c==0x0f:
            pass # SI
        elif c==0x10:
            pass # DLE
        elif c==0x11:
            self.fg = self.RED
        elif c==0x12:
            self.fg = self.GREEN
        elif c==0x13:
            self.fg = self.YELLOW
        elif c==0x14:
            self.fg = self.BLUE
        elif c==0x15:
            self.fg = self.MAGENTA
        elif c==0x16:
            self.fg = self.CYAN
        elif c==0x17:
            self.fg = self.WHITE

        elif c==0x18:
            self.flags.add(self.CONCEAL)
            # this can only be removed by newline

        elif c==0x19:
            self.flags.remove(self.SEP)
        elif c==0x1a:
            self.flags.add(self.SEP)

        elif c==0x1b:
            pass # ESC

        elif c==0x1c:
            self.bg = self.BLACK
        elif c==0x1d:
            self.bg = self.fg

        elif c==0x1e:
            self.flags.add(self.HOLD)
        elif c==0x1f:
            self.flags.remove(self.HOLD)

        return classes_before!=self.classes

    def normalise_char(self, codepoint):

        if codepoint >= 0xee00 and codepoint <= 0xee7f:
            pattern = codepoint & 0x5f
        elif codepoint < 0x80 and (codepoint & 0x20)!=0:
            pattern = codepoint & 0x5f
        elif codepoint in (0x7f, self.BLOCK_SYMBOL):
            pattern = 0x5f
        else:
            return codepoint

        if self.GRAPHIC not in self.flags:
            if pattern==0x5f:
                return chr(self.BLOCK_SYMBOL)
            else:
                return chr(0x20+pattern)

        elif self.SEP in self.flags:
            pattern |= 0x20
        else:
            pattern &= ~0x20

        return chr(0xee00 | pattern)
