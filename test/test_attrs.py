from teletext.attrs import Attrs

SYMBOLS = {
        0: (
            " !\"#$%&'()*+,-./0123456789:;<=>?"
            "@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_"
            "`abcdefghijklmnopqrstuvwxyz{|}~∎"
            ),

        1: (
            #      !"#$%&'()*+,-./0123456789:;<=>?
            "\uee00"
            "@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_"
            #`abcdefghijklmnopqrstuvwxyz{|}~∎
            ""
            ),

        2: (
            #      !"#$%&'()*+,-./0123456789:;<=>?
            "\uee20"
            "@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_"
            #`abcdefghijklmnopqrstuvwxyz{|}~∎
            ""
            ),
        }

def test_begins_in_default_state():
    a = Attrs()
    assert a.classes == 'b0 f7'

def test_change_fg_colour():
    a = Attrs()
    assert a.classes == 'b0 f7'

    a.handle(0x01)
    assert a.classes == 'b0 f1'

def test_change_bg_colour():
    a = Attrs()
    assert a.classes == 'b0 f7'

    a.handle(0x01)
    assert a.classes == 'b0 f1'

    a.handle(0x1d)
    assert a.classes == 'b1 f1'

    a.handle(0x06)
    assert a.classes == 'b1 f6'

    a.handle(0x1c)
    assert a.classes == 'b0 f6'

def test_counts_columns_and_resets_at_eol():

    a = Attrs()
    assert a.column == 0
    assert a.row == 0
    assert a.classes == 'b0 f7'

    a.handle(0x02)
    assert a.column== 1
    assert a.row== 0
    assert a.classes == 'b0 f2'

    run_to_next_line(a)

    assert a.column== 0
    assert a.row== 1
    assert a.classes == 'b0 f7'

def test_state_change_gives_true():

    a = Attrs()
    assert a.handle(0x02) == True
    assert a.handle(0x02) == False
    assert a.handle(0x03) == True
    assert a.handle(0x03) == False
    assert a.handle(0x02) == True

def test_dh_classes_alternate():

    a = Attrs()

    for i in range(7):

        # 0: d1 (double height, top)
        # 1: d2 (double height, bottom)
        # 2: d1
        # 3: d2
        # 4: neither
        # 5: d1
        # 6: d2

        if i==0 or i==2 or i==5:
            dh_classes = 'b0 d1 f7'
        elif i==1 or i==3 or i==6:
            dh_classes = 'b0 d2 f7'
        else:
            dh_classes = 'b0 f7'

        a.handle(0x30)
        assert a.classes == 'b0 f7', i

        if i==4:
            a.handle(0x20) # just a placeholder space
        else:
            a.handle(0x0d) # double height

        assert a.classes == dh_classes, i

        a.handle(0x0c)
        assert a.classes == 'b0 f7', i

        run_to_next_line(a)

def run_to_next_line(a):
    start_classes = a.classes
    for i in range(a.column, 39):
        found = a.handle(0x30)
        assert not found
        assert a.column == i+1
        assert a.classes == start_classes

    a.handle(0x30)
