import glob
import os
import teletext.unrender

FORMS = [
        ('b', 'braille'),
        ('z', 'zvbi'),
        ('t', 'teletext'),
        ('T', 'teletext-printable'),
        ]

def main():
    found = []
    found.extend(glob.glob("*.png"))
    found.extend(glob.glob("*.jpg"))
    found.extend(glob.glob("*.gif"))

    if not found:
        print('I found no image files in the current directory.')
        return

    for g in found:
        image = teletext.unrender.TeletextImage(g)

        for letter, keyword in FORMS:
            filename = f'{g}.{letter}.txt'
            try:
                with open(filename, 'w') as f:
                    print(filename, '...', end='')
                    f.write(image.as_text(form=keyword))
                print('done.')
            except Exception as e:
                print(e)

if __name__=='__main__':
    main()
