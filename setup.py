import os
import re
from io import open
from setuptools import setup

here = os.path.abspath(os.path.dirname(__file__))
README = open('README.md', 'r', encoding='utf-8').read()

def get_version(package):
    """
    Return package version as listed in `__version__` in `init.py`.
    """
    init_py = open(os.path.join(package, '__init__.py')).read()
    return re.search("__version__ = ['\"]([^'\"]+)['\"]", init_py).group(1)

version = get_version('teletext')

setup(
        name='teletext',
        version=version,
        url='https://gitlab.org/marnanel/teletext',
        license='MIT',
        description="interpret Teletext codes",
        long_description=README,
        long_description_content_type='text/markdown',
        author='Marnanel Thurman',
        author_email='marnanel@thurman.org.uk',
        packages=[
            'teletext',
            'teletext.unrender',
            ],
        include_package_data=True,
        install_requires=[
            'argparse',
            'pillow',
            ],
        python_requires=">=3.0",
        zip_safe=False, # for now, anyway
        classifiers=[
            'Development Status :: 2 - Pre-Alpha',
            ],
        entry_points={
            'console_scripts': [
                'teletext=teletext.__main__:main',
                'teletext-unrender=teletext.unrender.__main__:main',
                ],
            },
        )
